import unittest

from flaskapp.app import app


class BasicTest(unittest.TestCase):
    def setUp(self) -> None:
        app.config['TESTING'] = True
        self.client = app.test_client()

    def test_application(self):
        self.assertIsNotNone(self.client)
        response = self.client.get('/', follow_redirects=True)
        # print(dir(response), response.status_code, response.get_json())
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.get_json(), dict)

    def test_application_response(self):
        text2echo = 'ښYou are going to echo this'
        response = self.client.get(f'/echo?string={text2echo}',
                                   follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        response_dict = response.get_json()
        self.assertEqual(text2echo, response_dict.get('string'))


if __name__ == '__main__':
    unittest.main()
