import unittest

from flaskapp.app import app
from flaskapp.operations.functions import addus


class BasicTest(unittest.TestCase):

    def test_test(self):
        self.assertEqual(3, addus(1, 2))

if __name__ == '__main__':
    unittest.main()
