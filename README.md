

# run application
python -m flaskapp

# run tox
tox . -e tests,flake8

# run tests
python -m unittest


## HOW TO:
* create setup.py in pycharm: https://www.jetbrains.com/help/pycharm/creating-and-running-setup-py.html

