from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectMultipleField
from wtforms.fields.html5 import IntegerRangeField
from wtforms.validators import DataRequired


class TestForm(FlaskForm):
    text = StringField('TEXT',
                       validators=[
                           DataRequired(message='Text field required!!')],
                       description="The description",
                       default="default value",
                       id="txtfield"
                       )
    volume = IntegerRangeField('VOLUME')
    select = SelectMultipleField('Selectme',
                         choices=[],
                         coerce=int,  # coerce is necessary
                         )
    submit = SubmitField('SUBMIT',)
