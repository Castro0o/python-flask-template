import os
from flask import Flask
from flask import (jsonify, request, render_template, redirect, session)
from datetime import datetime
from flaskapp.operations.config import Config
from flaskapp.operations.forms import TestForm

app = Flask(__name__)
app.config.from_object(Config)
app.config['SECRET_KEY']


@app.route('/')
def root():
    _dict = {'app': __name__,
             'pwd': os.path.abspath(os.curdir)}
    json = jsonify(_dict)
    return json


@app.route('/echo')
def echo():
    json = jsonify({
        'string': request.args.get('string'),
        'date': datetime.now().isoformat()
    })
    print(session)
    return json

@app.route('/form', methods=['GET', 'POST'])
def form():
    if request.method == 'GET':
        return render_template('form.html', session=session)

    elif request.method == 'POST':
        session.update(request.form)
        return redirect('form')
# to facilitate the reuse of the values input by the user to the HTML form
# I am updating the session with the the request.form K:Vs
# since the session stores information specific to a user from one request to the next.


@app.route('/autoform', methods=['GET', 'POST'])
def autoform():
    # with wtforms the values posted
    # are "remember" back when the template is re-rendered
    # dispensing the use of session

    _form = TestForm()
    pchoices = [(i, i) for i in range(4)]
    _form.select.choices = pchoices
    if _form.validate_on_submit():
        print(_form.select.data)
        return render_template('autoform.html', form=_form)
    return render_template('autoform.html', form=_form)



if __name__ == '__main__':
    app.run(debug=True)