from setuptools import setup

setup(
    name='flaskapp',
    version='0.0.1',
    packages=['tests', 'flaskapp', 'flaskapp.operations'],
    url='',
    license='GNU GENERAL PUBLIC LICENSE V.3',
    author='Andre',
    author_email='',
    description='A Sandbox for a Flask App'
)
